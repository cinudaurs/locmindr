package com.dolatr.app.account;

import javax.inject.Inject;

import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.UserProfile;

import com.dolatr.app.documents.UserAccount;
import com.dolatr.app.repository.UserAccountRepository;
import com.dolatr.app.system.CounterService;

/**
 * Implementation for UserAdminService.
 */
public class UserAdminServiceImpl implements UserAdminService {
    public static final String USER_ID_PREFIX = "user";

    private final UserAccountRepository accountRepository;
    private final CounterService counterService;

    @Inject
    public UserAdminServiceImpl(UserAccountRepository accountRepository, CounterService counterService) {
        this.accountRepository = accountRepository;
        this.counterService = counterService;
    }


    @Override
    public UserAccount createUserAccount(ConnectionData data, UserProfile profile) {
        long userIdSequence = this.counterService.getNextUserIdSequence();
        UserAccount account = new UserAccount();
        account.setEmail(profile.getEmail());
        account.setUserId(USER_ID_PREFIX + userIdSequence);
        account.setDisplayName(data.getDisplayName());
        account.setImageUrl(data.getImageUrl());
        if (userIdSequence == 1l) {
            account.setRoles(new UserRoleType[] { UserRoleType.ROLE_USER, UserRoleType.ROLE_AUTHOR,
                    UserRoleType.ROLE_ADMIN });
        } else {
            account.setRoles(new UserRoleType[] { UserRoleType.ROLE_USER });
        }
        this.accountRepository.save(account);

        return account;
    }

    @Override
    public String getUserId() {
    	
    	AccountUtils accountUtils = new AccountUtils();
    	
        return accountUtils.getLoginUserId();
    }


    @Override
    public UserAccount getCurrentUser() {
        return accountRepository.findByUserId(getUserId());
    }


	@Override
	public UserAccount createUserAccount(String username, String password) {
		
		long userIdSequence = this.counterService.getNextUserIdSequence();
        UserAccount account = new UserAccount();
        account.setUserId(USER_ID_PREFIX + userIdSequence);
        account.setUsername(username);
        account.setPassword(password);
        
        if (userIdSequence == 1l) {
            account.setRoles(new UserRoleType[] { UserRoleType.ROLE_USER, UserRoleType.ROLE_AUTHOR,
                    UserRoleType.ROLE_ADMIN });
        } else {
            account.setRoles(new UserRoleType[] { UserRoleType.ROLE_USER });
        }
        this.accountRepository.save(account);

        return account;
		
	}

}
