package com.dolatr.app.config;

import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ServletWebArgumentResolverAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.dolatr.app.controller")
@ImportResource("classpath:security.xml")
public class WebConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        PageableArgumentResolver resolver = new PageableArgumentResolver();
        resolver.setFallbackPagable(new PageRequest(1, 10));
        argumentResolvers.add(new ServletWebArgumentResolverAdapter(resolver));
    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}
    
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/WEB-INF/messages/messages");
		return messageSource;
	}
	
//	@Bean
//	public ViewResolver viewResolver(SpringTemplateEngine templateEngine) {
//		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//		viewResolver.setTemplateEngine(templateEngine);
//		return viewResolver;
//	}
//	
//	@Bean
//	public SpringTemplateEngine templateEngine(TemplateResolver templateResolver) {
//		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//		templateEngine.setTemplateResolver(templateResolver);
//		templateEngine.addDialect(new StandardDialect());
//		templateEngine.addDialect(new LayoutDialect());
//		return templateEngine;
//	}
//	
//	
//	@Bean
//	public TemplateResolver templateResolver() {
//		TemplateResolver templateResolver = new ClassLoaderTemplateResolver();
//		templateResolver.setPrefix("/views/");
//		templateResolver.setSuffix(".html");
//		Set<String> newHtml5TemplatesModePatterns = new HashSet<String>();
//		newHtml5TemplatesModePatterns.add("HTML5");
//		templateResolver.setHtml5TemplateModePatterns(newHtml5TemplatesModePatterns );
//		return templateResolver;
//	}
   
        
    }

