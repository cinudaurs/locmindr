//package com.dolatr.app.controller;
//
//import javax.inject.Inject;
//
//import org.springframework.social.connect.ConnectionFactoryLocator;
//import org.springframework.social.connect.ConnectionRepository;
//import org.springframework.social.connect.web.ConnectController;
//import org.springframework.stereotype.Controller;
//
///**
// * Override Spring Social {@link ConnectController} to redirect to account overview page
// * after user connected or not connected to provider.
// */
//@Controller
//public class MyConnectController extends ConnectController{
//    
//	@Inject
//    public MyConnectController(
//			ConnectionFactoryLocator connectionFactoryLocator,
//			ConnectionRepository connectionRepository) {
//		super(connectionFactoryLocator, connectionRepository);
//		}
//
//	@Inject
//    public ConnectController connctCntrllr(ConnectionFactoryLocator connectionFactoryLocator,
//            ConnectionRepository connectionRepository) {
//       
//        ConnectController connectController = new ConnectController(connectionFactoryLocator, connectionRepository);
//		connectController.addInterceptor(new TweetAfterConnectInterceptor());
//		return connectController;
//    }
//       
//}
